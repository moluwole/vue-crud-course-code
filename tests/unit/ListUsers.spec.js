import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import ListUsers from '@/components/ListUsers.vue'

// Create a local Vue object for you to add components and plugins,
// without polluting the global Vue object
const localVue = createLocalVue()

// Utilize Vuex in the local Vue object
localVue.use(Vuex)


describe('ListUsers.vue Test when user data is read-only', () => {
  let wrapper = null
  let getters = null
  let actions = null
  let store = null

  // SETUP - run prior to each unit test
  beforeEach(() => {
    // Set the mock response to the 'getAllUsers' getter from the Vuex Store
    getters = {
      getAllUsers: () => [
        {
          id: 1,
          name: 'Test User #1',
          username: 'user_1',
          email: 'test1@gmail.com',
          editing: false
        },
        {
          id: 2,
          name: 'Test User #2',
          username: 'user_2',
          email: 'test2@gmail.com',
          editing: false
        },
        {
          id: 3,
          name: 'Test User #3',
          username: 'user_3',
          email: 'test3@gmail.com',
          editing: false
        }
      ]
    }

    // Set the mock call to the 'addNewUser' action in the Vuex Store
    actions = {
      deleteUser: jest.fn(),
      setUserToEditingMode: jest.fn(),
      setUserToReadOnlyMode: jest.fn(),
      updateUser: jest.fn()
    }

    // Create a temporary instance of the Vuex Store
    store = new Vuex.Store({
      getters,
      actions
    })

    // render the component
    wrapper = shallowMount(ListUsers, { localVue, store })
  })

  // TEARDOWN - run after to each unit test
  afterEach(() => {
    wrapper.destroy()
    jest.resetModules()
    jest.clearAllMocks()
  })

  it('renders a table of users when component is created', () => {
    // check the name of the component
    expect(wrapper.name()).toMatch('ListUsers')

    // check that the heading text is rendered
    expect(wrapper.findAll('h1').length).toEqual(1)
    expect(wrapper.findAll('h1').at(0).text()).toMatch('List of Users:')

    // check that 4 columns are created in the table
    expect(wrapper.findAll('th').length).toEqual(5)
    expect(wrapper.findAll('th').at(0).text()).toMatch('User ID')
    expect(wrapper.findAll('th').at(1).text()).toMatch('Name')
    expect(wrapper.findAll('th').at(2).text()).toMatch('Username')
    expect(wrapper.findAll('th').at(3).text()).toMatch('Email')
    expect(wrapper.findAll('th').at(4).text()).toMatch('Actions')

    wrapper.vm.$nextTick().then(function () {
      // check that 3 user rows with 4 columns each are created in the table
      expect(wrapper.findAll('td').length).toEqual(15)
      expect(wrapper.findAll('td').at(0).text()).toMatch('1')
      expect(wrapper.findAll('td').at(1).text()).toMatch('Test User #1')
      expect(wrapper.findAll('td').at(2).text()).toMatch('user_1')
      expect(wrapper.findAll('td').at(3).text()).toMatch('test1@gmail.com')
      expect(wrapper.findAll('td').at(4).text()).toMatch('Delete')
      expect(wrapper.findAll('td').at(5).text()).toMatch('2')
      // ...
      expect(wrapper.findAll('td').at(13).text()).toMatch('test3@gmail.com')
      expect(wrapper.findAll('td').at(14).text()).toMatch('Delete')

      // check that the Delete and Edit buttons are displayed, but
      // the Cancel and Update buttons are not displayed
      expect(wrapper.findAll('button').length).toEqual(12)
      expect(wrapper.findAll('#deleteButton').at(0).isVisible()).toBe(true)
      expect(wrapper.findAll('#editButton').at(0).isVisible()).toBe(true)
      expect(wrapper.findAll('#cancelEditButton').at(0).isVisible()).toBe(false)
      expect(wrapper.findAll('#updateButton').at(0).isVisible()).toBe(false)
    })
  })

  it('dispatches an action when a user is deleted', () => {
    // trigger an event when the 'Submit' button is clicked
    wrapper.find('#deleteButton').trigger('click', { user: { 'id': 0, 'name': 'Name1', 'username': 'Username1', 'email': 'Email1' } })

    wrapper.vm.$nextTick().then(function () {
      // check that the deleteUser action was dispatched
      expect(actions.deleteUser.mock.calls).toHaveLength(1)
    })
  })

  it('dispatches an action when a user is being edited', () => {
    // trigger an event when the 'Edit' button is clicked
    wrapper.find('#editButton').trigger('click', { user: { 'id': 1, 'name': 'Name1', 'username': 'Username1', 'email': 'Email1' } })

    wrapper.vm.$nextTick().then(function () {
      // check that the setUserToEditingMode action was dispatched
      expect(actions.setUserToEditingMode.mock.calls).toHaveLength(1)
      expect(actions.setUserToEditingMode.mock.calls[0][1]).toEqual(1)

      // check that the edit values are set based on the user data
      expect(wrapper.vm.inputId).toEqual(1)
      expect(wrapper.vm.inputName).toMatch('Test User #1')
      expect(wrapper.vm.inputUsername).toMatch('user_1')
      expect(wrapper.vm.inputEmail).toMatch('test1@gmail.com')
    })
  })
})

describe('ListUsers.vue Test when user data is editable', () => {
  let wrapper = null
  let getters = null
  let actions = null
  let store = null

  // SETUP - run prior to each unit test
  beforeEach(() => {
    // Set the mock response to the 'getAllUsers' getter from the Vuex Store
    getters = {
      getAllUsers: () => [
        {
          id: 1,
          name: 'Test User #1',
          username: 'user_1',
          email: 'test1@gmail.com',
          editing: true
        },
        {
          id: 2,
          name: 'Test User #2',
          username: 'user_2',
          email: 'test2@gmail.com',
          editing: false
        },
        {
          id: 3,
          name: 'Test User #3',
          username: 'user_3',
          email: 'test3@gmail.com',
          editing: false
        }
      ]
    }

    // Set the mock call to the 'addNewUser' action in the Vuex Store
    actions = {
      deleteUser: jest.fn(),
      setUserToEditingMode: jest.fn(),
      setUserToReadOnlyMode: jest.fn(),
      updateUser: jest.fn()
    }

    // Create a temporary instance of the Vuex Store
    store = new Vuex.Store({
      getters,
      actions
    })

    // render the component
    wrapper = shallowMount(ListUsers, { localVue, store })
  })

  // TEARDOWN - run after to each unit test
  afterEach(() => {
    wrapper.destroy()
    jest.resetModules()
    jest.clearAllMocks()
  })

  it('renders a table of users when component is created', () => {
    // check the name of the component
    expect(wrapper.name()).toMatch('ListUsers')

    // check that the heading text is rendered
    expect(wrapper.findAll('h1').length).toEqual(1)
    expect(wrapper.findAll('h1').at(0).text()).toMatch('List of Users:')

    // check that 4 columns are created in the table
    expect(wrapper.findAll('th').length).toEqual(5)
    expect(wrapper.findAll('th').at(0).text()).toMatch('User ID')
    expect(wrapper.findAll('th').at(1).text()).toMatch('Name')
    expect(wrapper.findAll('th').at(2).text()).toMatch('Username')
    expect(wrapper.findAll('th').at(3).text()).toMatch('Email')
    expect(wrapper.findAll('th').at(4).text()).toMatch('Actions')

    wrapper.vm.$nextTick().then(function () {
      // check that the Delete and Edit buttons are not displayed, but
      // the Cancel and Update buttons are displayed
      expect(wrapper.findAll('button').length).toEqual(12)
      expect(wrapper.findAll('#deleteButton').at(0).isVisible()).toBe(true)
      expect(wrapper.findAll('#editButton').at(0).isVisible()).toBe(false)
      expect(wrapper.findAll('#cancelEditButton').at(0).isVisible()).toBe(true)
      expect(wrapper.findAll('#updateButton').at(0).isVisible()).toBe(true)
      expect(wrapper.findAll('#deleteButton').at(1).isVisible()).toBe(true)
      expect(wrapper.findAll('#editButton').at(1).isVisible()).toBe(true)
      expect(wrapper.findAll('#cancelEditButton').at(1).isVisible()).toBe(false)
      expect(wrapper.findAll('#updateButton').at(1).isVisible()).toBe(false)
    })
  })

  it('dispatches an action when cancelling that a user is being edited', () => {
    // trigger an event when the 'Cancel' button is clicked
    wrapper.find('#cancelEditButton').trigger('click', { user: { 'id': 1, 'name': 'Test User #1', 'username': 'user_1', 'email': 'test1@gmail.com' } })

    wrapper.vm.$nextTick().then(function () {
      // check that the setUserToReadOnlyMode action was dispatched
      expect(actions.setUserToReadOnlyMode.mock.calls).toHaveLength(1)
      expect(actions.setUserToReadOnlyMode.mock.calls[0][1]).toEqual(1)

      // check that the input data is cleared after emitting the event
      expect(wrapper.vm.inputId).toBe(0)
      expect(wrapper.vm.inputName).toMatch(/^$/)
      expect(wrapper.vm.inputUsername).toMatch(/^$/)
      expect(wrapper.vm.inputEmail).toMatch(/^$/)
    })
  })

  it('dispatches an action when updating a user', () => {
    // set the input data
    wrapper.vm.inputId = 2
    wrapper.vm.inputName = 'name2'
    wrapper.vm.inputUsername = 'username2'
    wrapper.vm.inputEmail = 'user2@email.com'

    // trigger an event when the 'Cancel' button is clicked
    wrapper.find('#updateButton').trigger('click', { index: 0 })

    wrapper.vm.$nextTick().then(function () {
      // check that the updateUser action was dispatched
      expect(actions.updateUser.mock.calls).toHaveLength(1)
      expect(actions.updateUser.mock.calls[0][1]).toEqual(
        {
          index: 0,
          id: 2,
          name: 'name2',
          username: 'username2',
          email: 'user2@email.com'
        }
      )

      // check that the input data is cleared after emitting the event
      expect(wrapper.vm.inputId).toBe(0)
      expect(wrapper.vm.inputName).toMatch(/^$/)
      expect(wrapper.vm.inputUsername).toMatch(/^$/)
      expect(wrapper.vm.inputEmail).toMatch(/^$/)
    })
  })
})
