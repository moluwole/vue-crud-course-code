import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // Message to display on banner
    bannerMessage: '',
    // Banner Types: Info, Error, or Success
    bannerType: 'Info',
    // Each user object should contain the following fields:
    //  * id: integer
    //  * name: string
    //  * username: string
    //  * email: string
    //  * editing: boolean
    users: [],
    // Largest index used in the user array
    largestUserIndex: 0
  },
  getters: {
    getBannerMessage: (state) => {
      return state.bannerMessage;
    },
    getBannerType: (state) => {
      return state.bannerType;
    },
    getAllUsers: (state) => {
      return state.users;
    },
    getLargestUserIndex: (state) => {
      return state.largestUserIndex;
    }
  },
  mutations: {
    setBannerMessage: (state, payload) => {
      state.bannerMessage = payload;
    },
    setBannerType: (state, payload) => {
      state.bannerType = payload;
    },
    setUsers: (state, payload) => {
      state.users = payload;
    },
    appendUser: (state, payload) => {
      state.users.push(payload);
    },
    deleteUser: (state, payload) => {
      state.users.splice(payload, 1)
    },
    updateUser: (state, payload) => {
      state.users[payload.index].name = payload.name
      state.users[payload.index].username = payload.username
      state.users[payload.index].email = payload.email
      state.users[payload.index].editing = false
    },
    setUserToEditingMode: (state, payload) => {
      state.users[payload].editing = true
    },
    setUserToReadOnlyMode: (state, payload) => {
      state.users[payload].editing = false
    },
    setLargestUserIndex: (state, payload) => {
      state.largestUserIndex = payload;
    },
    incrementLargestUserIndex: (state) => {
      state.largestUserIndex++;
    }
  },
  actions: {
    setBanner: (context, payload) => {
      context.commit('setBannerMessage', payload.message)
      context.commit('setBannerType', payload.type)
    },
    retrieveUsers: (context) => {
      // Check if the users array in the Vuex Store is empty, which
      // indicates that the user data needs to be retrieved
      if (context.getters.getAllUsers.length === 0) {
        // GET request for user data
        axios.get('https://jsonplaceholder.typicode.com/users')
          .then((response) => {
            // handle success
            context.commit('setBannerMessage', 'SUCCESS! Loaded user data!')
            context.commit('setBannerType', 'Success')

            // Add the 'editing' field to each user object
            var mountedUsers = response.data.map(function (user) {
              user.editing = false
              return user
            })

            context.commit('setUsers', mountedUsers)
            context.commit('setLargestUserIndex', mountedUsers.length)
          })
          .catch((error) => {
            // handle error
            context.commit('setBannerMessage', 'ERROR! Unable to load user data!')
            context.commit('setBannerType', 'Error')

            console.log(error.message)
          })
          .finally((response) => {
            // always executed
            console.log('HTTP GET Finished!')
          })
      } else {
        console.log('Using existing user data!')
      }
    },
    addNewUser: (context, payload) => {
      var newUser = {
        // LIMITATION: The ID of the new user should really be returned
        //             from the server in the response to the POST call
        id: context.getters.getLargestUserIndex + 1,
        name: payload.name,
        username: payload.username,
        email: payload.email,
        editing: false
      }

      // Add the new user to the database via a HTTP POST call
      axios.post('https://jsonplaceholder.typicode.com/users', newUser)
        .then((response) => {
          // handle success
          context.commit('setBannerMessage', 'SUCCESS! User data was saved!')
          context.commit('setBannerType', 'Success')

          // Add the user to the local array of users
          context.commit('appendUser', newUser)

          // Increase the largest index used in the database
          context.commit('incrementLargestUserIndex')
        })
        .catch((error) => {
          // handle error
          context.commit('setBannerMessage', 'ERROR! Unable to save user data!')
          context.commit('setBannerType', 'Error')
          console.log(error.message)
        })
        .finally((response) => {
          // always executed
          console.log('HTTP POST Finished!')
        })
    },
    deleteUser: (context, payload) => {
      // Find the user
      var userIndex = context.getters.getAllUsers.indexOf(payload)

      // Delete the user from the database via a HTTP DELETE call
      axios.delete('https://jsonplaceholder.typicode.com/users/' + payload.id)
        .then((response) => {
          // handle success
          context.commit('setBannerMessage', 'SUCCESS! User #' + payload.id + ' was deleted!')
          context.commit('setBannerType', 'Success')

          // Delete the user from the local array of users
          context.commit('deleteUser', userIndex)
        })
        .catch((error) => {
          // handle error
          context.commit('setBannerMessage', 'ERROR! Unable to delete user #' + payload.id + '!')
          context.commit('setBannerType', 'Error')
          console.log(error.message)
        })
        .finally((response) => {
          // always executed
          console.log('HTTP DELETE Finished!')
        })
    },
    updateUser: (context, payload) => {
      // The argument passed in is a custom object with the following fields:
      //   - index: the index of the user within the array (not the user ID)
      //   - id: the ID number of the user (as stored in the database)
      //   - name: name of the user
      //   - username: username of the user
      //   - email: email of the user

      // Update the user in the database via a HTTP PUT call
      axios.put('https://jsonplaceholder.typicode.com/users/' + payload.id)
        .then((response) => {
          // handle success
          context.commit('setBannerMessage', 'SUCCESS! User #' + payload.id + ' was updated!')
          context.commit('setBannerType', 'Success')
          context.commit('updateUser', payload)
        })
        .catch((error) => {
          // handle error
          context.commit('setBannerMessage', 'ERROR! Unable to update user #' + payload.id + '!')
          context.commit('setBannerType', 'Error')
          console.log(error.message)

          // Update the user in the local array of users
          context.commit('setUserToReadOnlyMode', payload.index)
        })
        .finally((response) => {
          // always executed
          console.log('HTTP PUT Finished!')
        })
    },
    setUserToEditingMode: (context, payload) => {
      // Since only 1 user can be edited at a time, clear the editing flag
      // for all users
      for (var index = 0; index < context.getters.getAllUsers.length; index++) {
        // this.users[index].editing = false
        context.commit('setUserToReadOnlyMode', index)
      }

      // Find the user
      var userIndex = context.getters.getAllUsers.findIndex(i => i.id === payload)

      context.commit('setUserToEditingMode', userIndex)
    },
    setUserToReadOnlyMode: (context, payload) => {
      // Find the user
      var userIndex = context.getters.getAllUsers.findIndex(i => i.id === payload)

      context.commit('setUserToReadOnlyMode', userIndex)
    }
  }
})
