import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import AddNewUser from '@/components/AddNewUser.vue'

// Create a local Vue object for you to add components and plugins,
// without polluting the global Vue object
const localVue = createLocalVue()

// Utilize Vuex in the local Vue object
localVue.use(Vuex)


// Unit test suite for the AddNewUser component
describe('AddNewUser.vue Test', () => {
  let wrapper = null
  let actions = null
  let store = null

  // SETUP - run prior to each unit test
  beforeEach(() => {
    // Set the mock call to the 'addNewUser' action in the Vuex Store
    actions = {
      addNewUser: jest.fn()
    }

    // Create a temporary instance of the Vuex Store
    store = new Vuex.Store({
      actions
    })

    // render the component
    wrapper = shallowMount(AddNewUser, { localVue, store })
  })

  // TEARDOWN - run after to each unit test
  afterEach(() => {
    wrapper.destroy()
    jest.resetModules()
    jest.clearAllMocks()
  })

  it('initializes with correct elements', () => {
    // check the name of the component
    expect(wrapper.name()).toMatch('AddNewUser')

    // check that each element of the user is initialized to empty strings
    expect(wrapper.vm.user.name).toMatch('')
    expect(wrapper.vm.user.username).toMatch('')
    expect(wrapper.vm.user.email).toMatch('')

    // check that the heading text is rendered
    expect(wrapper.findAll('h1').length).toEqual(1)
    expect(wrapper.findAll('h1').at(0).text()).toMatch('Add a New User:')

    // check that 3 labels are created
    expect(wrapper.findAll('label').length).toEqual(3)
    expect(wrapper.findAll('label').at(0).text()).toMatch('Name:')
    expect(wrapper.findAll('label').at(1).text()).toMatch('Username:')
    expect(wrapper.findAll('label').at(2).text()).toMatch('Email:')
  })

  it('dispatches an action to the Vuex Store when a new user with valid data is added', () => {
    // set the input data for the user
    wrapper.setData({ user: { name: 'Name1', username: 'Username1', email: 'user@email.com' } })

    // check that the user data is set before emitting the event
    expect(wrapper.vm.user.name).toMatch('Name1')
    expect(wrapper.vm.user.username).toMatch('Username1')
    expect(wrapper.vm.user.email).toMatch('user@email.com')

    // trigger an event when the 'Submit' button is clicked
    wrapper.find('button').trigger('click')

    wrapper.vm.$nextTick().then(function () {
      // check that the addNewUser action was dispatched
      expect(actions.addNewUser.mock.calls).toHaveLength(1)
      expect(actions.addNewUser.mock.calls[0][1]).toEqual({ name: 'Name1', username: 'Username1', email: 'user@email.com' })

      // check that the user data is cleared after emitting the event
      expect(wrapper.vm.user.name).toMatch(/^$/)
      expect(wrapper.vm.user.username).toMatch(/^$/)
      expect(wrapper.vm.user.email).toMatch(/^$/)
    })
  })

  it('does not dispatch an action to the Vuex Store when a new user without data is added', () => {
    // don't set the input data for the user

    // trigger an event when the 'Submit' button is clicked
    wrapper.find('button').trigger('click')

    wrapper.vm.$nextTick().then(function () {
      // check that the addNewUser action was NOT dispatched
      expect(actions.addNewUser.mock.calls).toHaveLength(0)
    })
  })
})
