import { createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import store from '@/store'
import axios from 'axios'

// Create a local Vue object for you to add components and plugins,
// without polluting the global Vue object
const localVue = createLocalVue()

// Utilize Vuex in the local Vue object
localVue.use(Vuex)

// Mock the axios library
jest.mock('axios')

// Spy the console log
global.console.log = jest.fn();


//***************************
// Helper Functions
//***************************

// Helper function to add two users to the Vuex Store
function addTwoUsersToVuexStore() {
  var newUser1 = {
    id: 1,
    name: 'Leanne Graham',
    username: 'Bret',
    email: 'Sincere@april.biz',
    editing: false
  }

  var newUser2 = {
    id: 2,
    name: 'Ervin Howell',
    username: 'Antonette',
    email: 'Shanna@melissa.tv',
    editing: false
  }

  store.state.users.push(newUser1)
  store.state.users.push(newUser2)
}


//***************************
// Unit Test Suite(s)
//***************************

describe('Vuex Store Test', () => {
  beforeEach(() => {
    // Set the banner data
    store.state.bannerMessage = 'Temporary Message'
    store.state.bannerType = 'Info'

    // Clear the users data
    store.state.users = []
    store.state.largestUserIndex = 0
  })

  afterEach(() => {
    jest.resetModules()
    jest.clearAllMocks()
  })

  it('initializes with correct values', () => {
    expect(store.state.bannerMessage).toMatch('Temporary Message')
    expect(store.state.bannerType).toMatch('Info')
    expect(store.state.largestUserIndex).toBe(0)
  })

  it('test getters of the Vuex Store', () => {
    // Set the banner data
    store.state.bannerMessage = 'Successful Message'
    store.state.bannerType = 'Success'

    // Test the Getters
    expect(store.getters.getBannerMessage).toMatch('Successful Message')
    expect(store.getters.getBannerType).toMatch('Success')
  })

  it('test mutations of the Vuex Store', () => {
    // Set the banner data
    store.state.bannerMessage = 'Successful Message'
    store.state.bannerType = 'Success'

    // Call the mutations
    store.commit('setBannerMessage', 'Failed Message')
    store.commit('setBannerType', 'Failure')

    // Test the results
    expect(store.state.bannerMessage).toMatch('Failed Message')
    expect(store.state.bannerType).toMatch('Failure')
  })

  it('test actions of the Vuex Store', () => {
    // Set the banner data
    store.state.bannerMessage = 'Temporary Message'
    store.state.bannerType = 'Info'

    // Call the action
    store.dispatch('setBanner', {
      message: 'It worked!',
      type: 'Success' }
    )

    // Test the results
    expect(store.state.bannerMessage).toMatch('It worked!')
    expect(store.state.bannerType).toMatch('Success')
  })

  it('test the retrieveUsers action when the HTTP GET call is successful', () => {
    // Set the mock call to GET to return a successful GET response
    const responseGet = { data: [
      {
        id: 1,
        name: 'Leanne Graham',
        username: 'Bret',
        email: 'Sincere@april.biz'
      },
      {
        id: 2,
        name: 'Ervin Howell',
        username: 'Antonette',
        email: 'Shanna@melissa.tv'
      }
    ] }

    axios.get.mockResolvedValue(responseGet)

    // Call the action
    store.dispatch('retrieveUsers')

    localVue.nextTick().then(function () {
      expect(axios.get).toHaveBeenCalledTimes(1)
      expect(axios.get).toBeCalledWith('https://jsonplaceholder.typicode.com/users')

      // check that the user data is properly set
      expect(store.state.users.length).toEqual(2)
      expect(store.state.users[0].name).toMatch('Leanne Graham')
      expect(store.state.users[0].username).toMatch('Bret')
      expect(store.state.users[0].email).toMatch('Sincere@april.biz')
      expect(store.state.users[0].editing).toBe(false)
      expect(store.state.users[1].name).toMatch('Ervin Howell')
      expect(store.state.users[1].username).toMatch('Antonette')
      expect(store.state.users[1].email).toMatch('Shanna@melissa.tv')
      expect(store.state.users[1].editing).toBe(false)

      // check that the banner message indicates success
      expect(store.state.bannerMessage).toMatch('SUCCESS! Loaded user data!')
      expect(store.state.bannerType).toMatch('Success')
    })
  })

  it('test the retrieveUsers action when the HTTP GET call fails', () => {
    // Set the mock call to GET to return a failed GET request
    axios.get.mockRejectedValue(new Error('BAD REQUEST'))

    // Call the action
    store.dispatch('retrieveUsers')

    localVue.nextTick().then(function () {
      expect(axios.get).toHaveBeenCalledTimes(1)
      expect(axios.get).toBeCalledWith('https://jsonplaceholder.typicode.com/users')

      // Check that there is no user data loaded when the GET request fails
      expect(store.state.users.length).toEqual(0)

      // check that the banner message indicates failure
      expect(store.state.bannerMessage).toMatch('ERROR! Unable to load user data!')
      expect(store.state.bannerType).toMatch('Error')

      expect(global.console.log).toHaveBeenCalledWith('BAD REQUEST');
    })
  })

  it('test the addNewUser action when the HTTP POST call is successful', () => {
    // Set the mock call to POST to return a successful POST response
    const responsePost = { data: [
      {
        id: 0,
        name: 'Patrick',
        username: 'patrick123',
        email: 'patrick@email.com'
      }
    ] }

    axios.post.mockResolvedValue(responsePost)

    // Set the input data for user #3
    var newUser = {
      id: 1,
      name: 'Patrick',
      username: 'patrick123',
      email: 'patrick@email.com',
      editing: false
    }

    // Call the action
    store.dispatch('addNewUser', newUser)

    localVue.nextTick().then(function () {
      expect(axios.post).toHaveBeenCalledTimes(1)
      expect(axios.post).toBeCalledWith('https://jsonplaceholder.typicode.com/users', newUser)

      // Check that the new user was added when the POST call is successful
      expect(store.state.users.length).toEqual(1)

      // check that the banner message indicates success
      expect(store.state.bannerMessage).toMatch('SUCCESS! User data was saved!')
      expect(store.state.bannerType).toMatch('Success')
    })
  })

  it('test the addNewUser action when the HTTP POST call fails', () => {
    // Set the mock call to POST to return a failed POST request
    axios.post.mockRejectedValue(new Error('BAD CREATE'))

    // Set the input data for user #3
    var newUser = {
      id: 1,
      name: 'Patrick',
      username: 'patrick123',
      email: 'patrick@email.com',
      editing: false
    }

    // Call the action
    store.dispatch('addNewUser', newUser)

    localVue.nextTick().then(function () {
      expect(axios.post).toHaveBeenCalledTimes(1)
      expect(axios.post).toBeCalledWith('https://jsonplaceholder.typicode.com/users', newUser)

      // Check that there is no user data loaded when the POST request fails
      expect(store.state.users.length).toEqual(0)

      // Check that the banner message indicates failure
      expect(store.state.bannerMessage).toMatch('ERROR! Unable to save user data!')
      expect(store.state.bannerType).toMatch('Error')
      expect(global.console.log).toHaveBeenCalledWith('BAD CREATE')
    })
  })

  it('test the deleteUser action when the HTTP DELETE call is successful', () => {
    // Add a user to the Vuex Store
    addTwoUsersToVuexStore()

    // Set the mock call to DELETE to return a successful DELETE response
    const responseDelete = { data: [
      {
        id: 2
      }
    ] }

    // Set the mock call to DELETE to return a successful DELETE response
    axios.delete.mockResolvedValue(responseDelete)

    // Set the input data for the user to delete
    var deleteUser = {
      id: 2,
      name: 'Ervin Howell',
      username: 'Antonette',
      email: 'Shanna@melissa.tv'
    }

    // Call the action
    store.dispatch('deleteUser', deleteUser)

    localVue.nextTick().then(function () {
      expect(axios.delete).toHaveBeenCalledTimes(1)
      expect(axios.delete).toBeCalledWith('https://jsonplaceholder.typicode.com/users/2')

      // Check that the user was deleted when the DELETE call is successful
      expect(store.state.users.length).toEqual(1)

      // check that the banner message indicates success
      expect(store.state.bannerMessage).toMatch('SUCCESS! User #2 was deleted!')
      expect(store.state.bannerType).toMatch('Success')
    })
  })

  it('test the deleteUser action when the HTTP DELETE call fails', () => {
    // Add a user to the Vuex Store
    addTwoUsersToVuexStore()

    // Set the mock call to DELETE to return a failed DELETE request
    axios.delete.mockRejectedValue(new Error('BAD DELETE'))

    // Set the input data for the user to delete
    var deleteUser = {
      id: 2,
      name: 'Ervin Howell',
      username: 'Antonette',
      email: 'Shanna@melissa.tv'
    }

    // Call the action
    store.dispatch('deleteUser', deleteUser)

    localVue.nextTick().then(function () {
      expect(axios.delete).toHaveBeenCalledTimes(1)
      expect(axios.delete).toBeCalledWith('https://jsonplaceholder.typicode.com/users/2')

      // Check that the user was NOT deleted when the DELETE call fails
      expect(store.state.users.length).toEqual(2)

      // Check that the banner message indicates failure
      expect(store.state.bannerMessage).toMatch('ERROR! Unable to delete user #2!')
      expect(store.state.bannerType).toMatch('Error')
      expect(global.console.log).toHaveBeenCalledWith('BAD DELETE')
    })
  })

  it('test the updateUser action when the HTTP PUT call is successful', () => {
    // Add a user to the Vuex Store
    addTwoUsersToVuexStore()

    // Set the mock call to PUT to return a successful PUT response
    const responsePut = { data: [
      {
        id: 1,
        name: 'Patrick',
        username: 'patrick456',
        email: 'patrick@email.com'
      }
    ] }

    axios.put.mockResolvedValue(responsePut)

    // set the input data for the user to update
    var updateUser1 = {
      index: 1,
      id: 2,
      name: 'Patrick',
      username: 'patrick456',
      email: 'patrick@email.com'
    }

    // Call the action
    store.dispatch('updateUser', updateUser1)

    localVue.nextTick().then(function () {
      expect(axios.put).toHaveBeenCalledTimes(1)
      expect(axios.put).toBeCalledWith('https://jsonplaceholder.typicode.com/users/2')

      // Check that the user was updated when the PUT call is successful
      expect(store.state.users.length).toEqual(2)
      expect(store.state.users[0].name).toMatch('Leanne Graham')
      expect(store.state.users[0].username).toMatch('Bret')
      expect(store.state.users[0].email).toMatch('Sincere@april.biz')
      expect(store.state.users[0].editing).toBe(false)
      expect(store.state.users[1].name).toMatch('Patrick')
      expect(store.state.users[1].username).toMatch('patrick456')
      expect(store.state.users[1].email).toMatch('patrick@email.com')
      expect(store.state.users[1].editing).toBe(false)

      // check that the banner message indicates success
      expect(store.state.bannerMessage).toMatch('SUCCESS! User #2 was updated!')
      expect(store.state.bannerType).toMatch('Success')
    })
  })

  it('test the updateUser action when the HTTP PUT call fails', () => {
    // Add a user to the Vuex Store
    addTwoUsersToVuexStore()

    // Set the mock call to PUT to return a failed PUT request
    axios.put.mockRejectedValue(new Error('BAD PUT'))

    // set the input data for the user to update
    var updateUser1 = {
      index: 0,
      id: 1,
      name: 'Patrick',
      username: 'patrick456',
      email: 'patrick@email.com'
    }

    // Call the action
    store.dispatch('updateUser', updateUser1)

    localVue.nextTick().then(function () {
      expect(axios.put).toHaveBeenCalledTimes(1)
      expect(axios.put).toBeCalledWith('https://jsonplaceholder.typicode.com/users/1')

      // Check that the user was updated when the PUT call is successful
      expect(store.state.users.length).toEqual(2)
      expect(store.state.users[0].name).toMatch('Leanne Graham')
      expect(store.state.users[0].username).toMatch('Bret')
      expect(store.state.users[0].email).toMatch('Sincere@april.biz')
      expect(store.state.users[0].editing).toBe(false)
      expect(store.state.users[1].name).toMatch('Ervin Howell')
      expect(store.state.users[1].username).toMatch('Antonette')
      expect(store.state.users[1].email).toMatch('Shanna@melissa.tv')
      expect(store.state.users[1].editing).toBe(false)

      // check that the banner message indicates failure
      expect(store.state.bannerMessage).toMatch('ERROR! Unable to update user #1!')
      expect(store.state.bannerType).toMatch('Error')
    })
  })
})
