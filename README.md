## Course Synopsis

This course teaches the fundamentals of VueJS by building a CRUD (Create, Read, Update, Delete) website.

## Description

This tutorial provides an introduction to VueJS by building a website that manages a list of users. This type of site is called a CRUD (Create, Read, Update, and Delete) application, as it provides the key functionality for interacting with a database.

This tutorial is primarily focused on teaching the fundamentals of VueJS by building an application.  This application does NOT use the Vue CLI (Command Line Interface) tool, but focuses on VueJS fundamentals through a standard web page.

The styling of this application is done using CSS and CSS Grid.  I’ll discuss a bit about the styling, but it is not the focus of the tutorial.  Additionally, the database that would typically be used for storing the data is being replaced by a web application (https://jsonplaceholder.typicode.com) which provides a REST API for testing applications.

## Example

![Alt text](/screenshots/VueJS_CRUD_Tutorial.gif?raw=true "VueJs CRUD Example")

## Chapters

Part I (Vue Fundamentals)
* Chapter 1 - Introduction to VueJS
* Chapter 2 - Getting Started with VueJS
* Chapter 3 - Displaying Lists with the v-for Directive
* Chapter 4 - Using the v-on and v-show Directives
* Chapter 5 - Using the v-if and v-else Directives
* Chapter 6 - Introduction to Methods
* Chapter 7 - Method (Part II)
* Chapter 8 - Computed Properties
* Chapter 9 - Style Binding

Part II (Components)
* Chapter 10 - Introduction to Components
* Chapter 11 - Vue CLI
* Chapter 12 - Vue UI
* Chapter 13 - Components (Part II)
* Chapter 14 - Unit Testing
* Chapter 15 - Props
* Chapter 16 - Components (Part III)
* Chapter 17 - Custom Events
* Chapter 18 - Unit Testing (Part II)

Part III (Working with an API)
* Chapter 19 - Vue Lifecyle
* Chapter 20 - TDD: Loading data via GET
* Chapter 21 - TDD: Message Banner
* Chapter 22 - TDD: Saving data via POST
* Chapter 23 - TDD: Deleting data via DELETE
* Chapter 24 - TDD: Updating data via PUT

Part IV (Vue Router and Vuex)
* Chapter 25 - Vue Router
* Chapter 26 - Vuex
* Chapter 27 - Deploying to Netlify
* Chapter 28 - Conclusion

## Additional Resources

VueJS Documentation: https://vuejs.org/v2/guide/
