import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import axios from 'axios'
import Content from '@/components/Content.vue'

// Create a local Vue object for you to add components and plugins,
// without polluting the global Vue object
const localVue = createLocalVue()

// Utilize Vuex in the local Vue object
localVue.use(Vuex)

// Mock the axios library
jest.mock('axios')

// Spy the console log
global.console.log = jest.fn();


describe('Content.vue Test with Successful HTTP GET, POST, and Delete', () => {
  let wrapper = null
  let actions = null
  let store = null

  beforeEach(() => {
    // Set the mock call to the 'retrieveUsers' action in the Vuex Store
    actions = {
      retrieveUsers: jest.fn()
    }

    // Create a temporary instance of the Vuex Store
    store = new Vuex.Store({
      actions
    })

    // render the component
    wrapper = shallowMount(Content, { localVue, store })
  })

  afterEach(() => {
    wrapper.destroy()
    jest.resetModules()
    jest.clearAllMocks()
  })

  it('loads the user data when the component is created and mounted', () => {
    // check the name of the component
    expect(wrapper.name()).toMatch('Content')

    // check that the retrieveUsers action was dispatched
    expect(actions.retrieveUsers.mock.calls).toHaveLength(1)
  })
})
