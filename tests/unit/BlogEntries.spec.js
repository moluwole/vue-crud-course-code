import { shallowMount } from '@vue/test-utils'
import BlogEntries from '@/components/BlogEntries.vue'


describe('BlogEntries.vue Test', () => {
  it('renders three blog posts when component is created', () => {
    // render the component
    const wrapper = shallowMount(BlogEntries)

    // check the name of the component
    expect(wrapper.name()).toMatch('BlogEntries')

    // check that the blog post titles are rendered
    expect(wrapper.findAll('h2').length).toEqual(3)
    expect(wrapper.findAll('h2').at(0).text()).toMatch('My Favorite Aspects of Vue')
    expect(wrapper.findAll('h2').at(1).text()).toMatch('How to Use Components to Build Complex Web Applications')
    expect(wrapper.findAll('h2').at(2).text()).toMatch('Unit Testing a Vuex Store')
  })
})
